+++
title = "About"
description = "About me"
date = "2022-02-13"
aliases = ["about-us", "contact"]
author = "totallypredictable"
+++

I am a software developer and a machine learning engineer from Europe. This
website is mainly going to be about my random thoughts on software engineering,
philosophy, maths, cryptography, politics and artificial intelligence. 

Vulgar language is likely to happen. Everything I publish are solely my own
thoughts.

You can contact me at me@totallypredictable.xyz

Please encrypt your email with my public key below:

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: OpenPGP.js v4.10.10
Comment: https://openpgpjs.org

xjMEYghT+xYJKwYBBAHaRw8BAQdADnqNpG//ZDza0wBkijhXE19wn1KkQCZD
3OKX5xpeZtrNNW1lQHRvdGFsbHlwcmVkaWN0YWJsZS54eXogPG1lQHRvdGFs
bHlwcmVkaWN0YWJsZS54eXo+wo8EEBYKACAFAmIIU/sGCwkHCAMCBBUICgIE
FgIBAAIZAQIbAwIeAQAhCRBZ+k1N79SSRxYhBCd3QSXwW4wA+5bgaFn6TU3v
1JJHuVMBAJeJoU/SJ67SyZKwFZk3ueT+edO7EsHuhLcmyyyDyMS7AP9lciMU
HoM1p3AxF0PZw3wL1j2Yb8oYDILPANwSQ7dgBM44BGIIU/sSCisGAQQBl1UB
BQEBB0DdCiNwmW/CwcRsvARiIVuFH32GGK4zdZzloMQMuHjBMgMBCAfCeAQY
FggACQUCYghT+wIbDAAhCRBZ+k1N79SSRxYhBCd3QSXwW4wA+5bgaFn6TU3v
1JJHqNMBANrb5LOk+9fUQVl8aHi026aBb8ERJ/axPYPG9U5YOWyyAP0XFpu4
XoIFqOxIFyExVq1oxbPPSZRtQQYDgljJimsqCQ==
=IlSU
-----END PGP PUBLIC KEY BLOCK-----

```
